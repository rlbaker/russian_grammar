import random

from django.views import generic

from noun_declension.models import Noun


class NounView(generic.TemplateView):
    template_name = "noun.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        nouns = list(Noun.objects.all()[:200])
        noun = random.choice(nouns)

        context['noun'] = noun

        for decl in noun.declension_set.all():
            num = decl.grammatical_number
            case = decl.grammatical_case
            key = f'{num}_{case}'

            if key in context:
                context[key].append(decl)
            else:
                context[key] = [decl]

        # Table rendering helpers
        if 'singular_nominative' in context:
            context['has_singular'] = True

        if 'plural_nominative' in context:
            context['has_plural'] = True

        if 'singular_prepositional' in context or 'plural_prepositional' in context:
            context['has_prepositional'] = True

        if 'singular_locative' in context or 'plural_locative' in context:
            context['has_locative'] = True

        if 'singular_vocative' in context or 'plural_vocative' in context:
            context['has_vocative'] = True

        if 'singular_partitive' in context or 'plural_partitive' in context:
            context['has_partitive'] = True

        return context
