from django.db import models


# Create your models here.
class Noun(models.Model):
    word = models.TextField(unique=True)

    gender_choices = (
        ('masculine', 'Masculine'),
        ('feminine', 'Feminine'),
        ('neuter', 'Neuter'),
    )
    gender = models.TextField(choices=gender_choices)

    animacy_choices = (
        ('inanimate', 'Inanimate'),
        ('animate', 'Animate'),
        ('both', 'Both')
    )
    animacy = models.TextField(choices=animacy_choices)


class Declension(models.Model):
    noun = models.ForeignKey(Noun, on_delete=models.CASCADE)
    word = models.TextField()

    grammatical_number_choices = (
        ('singular', 'Singular'),
        ('plural', 'Plural')
    )
    grammatical_number = models.TextField(choices=grammatical_number_choices)

    grammatical_case_choices = (
        ('nominative', 'Nominative'),
        ('accusative', 'Accusative'),
        ('genitive', 'Genitive'),
        ('dative', 'Dative'),
        ('instrumental', 'Instrumental'),
        ('prepositional', 'Prepositional'),
        ('locative', 'Locative'),
        ('vocative', 'Vocative'),
        ('partitive', 'Partitive'),
    )
    grammatical_case = models.TextField(choices=grammatical_case_choices)

    animacy_choices = (
        ('inanimate', 'Inanimate'),
        ('animate', 'Animate'),
        ('both', 'Both')
    )
    animacy = models.TextField(choices=animacy_choices)
