from django.apps import AppConfig


class NounDeclensionConfig(AppConfig):
    name = 'noun_declension'
