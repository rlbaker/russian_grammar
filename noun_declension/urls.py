from django.conf.urls import url

from noun_declension import views

urlpatterns = [
    url(r'^$', views.NounView.as_view(), name='index'),
]
